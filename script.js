const link = document.getElementById("themeStylesheet");

const themeBtn = document.getElementById("theme-button");


function changeTheme() {
    const linkAttr = link.getAttribute("data-theme");

    if (linkAttr === "light-theme") {
        link.setAttribute("data-theme", "dark-theme");
    }   else {
        link.setAttribute("data-theme", "light-theme");
    }
    updateTheme(link.getAttribute("data-theme"));
    console.log("become", link.getAttribute("data-theme"));
};

function updateTheme(theme) {
    link.href = `css/${theme}.css`;
    localStorage.setItem("theme", theme);
};

const savedTheme = localStorage.getItem("theme");
console.log(savedTheme);

if (savedTheme) {
    link.setAttribute("data-theme", savedTheme)
    updateTheme(savedTheme);
};

themeBtn.addEventListener("click", changeTheme);



//!!! Theme button at the bottom (footer)